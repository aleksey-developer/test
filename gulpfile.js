var lr = require('tiny-lr'), // Минивебсервер для livereload
    gulp = require('gulp'), // Сообственно Gulp JS
    jade = require('gulp-jade'), // Плагин для Jade
    stylus = require('gulp-stylus'), // Плагин для Stylus
    livereload = require('gulp-livereload'), // Livereload для Gulp
    myth = require('gulp-myth'), // Плагин для Myth - http://www.myth.io/
    csso = require('gulp-csso'), // Минификация CSS
    imagemin = require('gulp-imagemin'), // Минификация изображений
    uglify = require('gulp-uglify'), // Минификация JS
    concat = require('gulp-concat'), // Склейка файлов
    connect = require('connect'), // Webserver
    serveStatic = require('serve-static'),
    server = lr();

// Собираем Stylus
gulp.task('stylus', function() {
    gulp.src('./assets/styles/base.styl')
        .pipe(stylus()) // собираем stylus (!!! Посмотреть Gulp Nib)
        .on('error', console.log) // Если есть ошибки, выводим и продолжаем
        .pipe(myth()) // добавляем префиксы - http://www.myth.io/
        .pipe(gulp.dest('./public/css/')) // записываем css
        .pipe(livereload(server)); // даем команду на перезагрузку css
});

// Собираем html из Jade
gulp.task('jade', function() {
    gulp.src(['./assets/template/*.jade', '!./assets/template/_*.jade'])
        .pipe(jade({
            pretty: true
        }))  // Собираем Jade только в папке ./assets/template/ исключая файлы с _*
        .on('error', console.log) // Если есть ошибки, выводим и продолжаем
        .pipe(gulp.dest('./public/')) // Записываем собранные файлы
        .pipe(livereload(server)); // даем команду на перезагрузку страницы
});

// Собираем JS
gulp.task('libsjs', function() {
    gulp.src(['./assets/javascripts/libs/jquery/*.js', './assets/javascripts/libs/**/*.js', '!./assets/javascripts/vendor/**/*.js', '!./assets/javascripts/app/**/*.js'])
        .pipe(concat('libs.js')) // Собираем все JS, кроме тех которые находятся в ./assets/js/vendor/**
        .pipe(gulp.dest('./public/js'))
        .pipe(livereload(server)); // даем команду на перезагрузку страницы
});
gulp.task('appjs', function() {
    gulp.src(['./assets/javascripts/app/**/*.js', '!./assets/javascripts/vendor/**/*.js'])
        .pipe(concat('app.js')) // Собираем все JS, кроме тех которые находятся в ./assets/js/vendor/**
        .pipe(gulp.dest('./public/js'))
        .pipe(livereload(server)); // даем команду на перезагрузку страницы
});

// Копируем и минимизируем изображения
gulp.task('images', function() {
    gulp.src('./assets/images/**/*')
        .pipe(imagemin())
        .pipe(gulp.dest('./public/img'))
});


// Локальный сервер для разработки
gulp.task('http-server', function() {
    connect()
        .use(serveStatic('./public'))
        .listen('5000');

    console.log('Server listening on http://localhost:5000');
});

// Запуск сервера разработки gulp watch ($ gulp watch)
gulp.task('watch', function() {
    // Предварительная сборка проекта
    gulp.run('stylus');
    gulp.run('jade');
    gulp.run('images');
    gulp.run('libsjs');
    gulp.run('appjs');

    // Подключаем Livereload
    server.listen(35729, function(err) {
        if (err) return console.log(err);

        gulp.watch('assets/styles/**/*.styl', function() {
            gulp.run('stylus');
        });
        gulp.watch('assets/template/**/*.jade', function() {
            gulp.run('jade');
        });
        gulp.watch('assets/images/**/*', function() {
            gulp.run('images');
        });
        gulp.watch('assets/javascripts/**/*', function() {
            gulp.run('libsjs');
            gulp.run('appjs');
        });
    });
    gulp.run('http-server');
});

// $ gulp build
gulp.task('build', function() {
    // css
    gulp.src('./assets/styles/base.styl')
        .pipe(stylus()) // собираем stylus
        .pipe(myth()) // добавляем префиксы - http://www.myth.io/
        .pipe(csso()) // минимизируем css
        .pipe(gulp.dest('./build/css/')); // записываем css

    // jade
    gulp.src(['./assets/template/*.jade', '!./assets/template/_*.jade'])
        .pipe(jade())
        .pipe(gulp.dest('./build/'));

    // js
    gulp.src(['./assets/javascripts/libs/jquery/*.js', './assets/javascripts/libs/**/*.js', '!./assets/javascripts/vendor/**/*.js', '!./assets/javascripts/app/**/*.js'])
        .pipe(concat('libs.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./build/js'));
    gulp.src(['./assets/javascripts/app/**/*.js', '!./assets/javascripts/vendor/**/*.js'])
        .pipe(concat('app.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./build/js'));

    // image
    gulp.src('./assets/images/**/*')
        .pipe(imagemin())
        .pipe(gulp.dest('./build/img'));

});

